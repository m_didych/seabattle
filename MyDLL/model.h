#ifndef MODEL_H
#define MODEL_H

#ifdef MYDLL_EXPORTS
#define MYDLL_API __declspec(dllexport)
#else
#define MYDLL_API __declspec(dllimport)
#endif

#include<vector>
#include<set>
#include<time.h>
class MYDLL_API Coordinates 
{
	public:

		typedef short unsigned int num_type;
	
		Coordinates():m_letter(10),m_number(10){}
		Coordinates(num_type letter,num_type number):m_letter(letter),m_number(number){}
		~Coordinates(){}

		bool operator<(const Coordinates other) const
		{
			if(m_number==other.get_number())
			{
				return m_letter<other.get_letter();
			}
			else
			{
				return m_number<other.get_number();
			}
		}

		void set_letter(num_type letter){m_letter=letter;}
		void set_number(num_type number){m_number=number;}
		num_type get_letter() const {return m_letter;}
		num_type get_number() const {return m_number;}

	private:

		num_type  m_letter;
		num_type  m_number;
};


class MYDLL_API Cell
{
	public:
		//constructs not revealed empty cell
		Cell():m_flags(4),m_ship_index(-1){}

		Cell(int init_flags):m_flags(init_flags){}

		~Cell(){}

		//Functions that verify some properties of cell
		bool isRevealed() const {return m_flags & CellFlags_Revealed;}
		bool isEmpty() const {return m_flags & CellFlags_Empty;}
		bool isHighlited() const {return m_flags & CellFlags_Highlited;}
		bool isHit() const {return m_flags & CellFlags_Hit;}
		bool isSunk() const {return m_flags & CellFlags_Sunk;}

		//Modifiers
		void reveal(){m_flags |= CellFlags_Revealed;}
		void makeEmpty(){m_flags |=CellFlags_Empty;}
		void hit(){m_flags |= CellFlags_Hit;}
		void sink(){m_flags |=CellFlags_Sunk;}
		void makeHighlited(){m_flags |= CellFlags_Highlited;}
		
		void makeNotRevealed(){m_flags &= ~CellFlags_Revealed;}
		void makeOccupied(){m_flags &= ~CellFlags_Empty;}
		void makeNotHit(){m_flags &= ~CellFlags_Hit;}
		void makeNotSunk(){m_flags &= ~CellFlags_Sunk;}
		void makeNotHighlited(){m_flags &= ~CellFlags_Highlited;}
		
		
		

		void setShipIndex(int index){m_ship_index=index;}
		int  getShipIndex(){return m_ship_index;}

	private:

		enum CellFlags
        {            
            CellFlags_Revealed       = 0x01,     // whether its content is revealed (1) or not (0)
            CellFlags_Empty          = 0x04,     // Whether cell is empty (1) or is part of a ship(0)
            CellFlags_Hit            = 0x08,     // Whether cell was hit(1) or not (0)            
            CellFlags_Sunk           = 0x10,     // Whether cell is part of sunk ship  (1) or not (0)
			CellFlags_Highlited      = 0x20,     // whether cell is highlited (1) or not (0)
        };

		int m_flags;
		int m_ship_index; 
};


class MYDLL_API GridModel
{
public:
	const static size_t size=10;
	typedef std::vector<Cell> cell_container;
	typedef std::set<Coordinates> changes_container;
	typedef std::vector<Coordinates> coordinates_container;
	

	enum ShipOrientation
	{
		ShipOrientation_Horisontal =0,
		ShipOrientation_Vertical   =1,
	};
	enum ShipType
	{
		ShipType_SingleDecker =1,
		ShipType_TwoDecker    =2,
		ShipType_ThreeDecker  =3,
		ShipType_FourDecker   =4,
	};

	class Ship
	{
		public: 
			typedef std::vector<Coordinates> deck_container;
			typedef short unsigned int num_type;

			Ship():m_decksNumber(0),
				   m_decks(deck_container()),
				   m_type(GridModel::ShipType_SingleDecker)
			{

			}

			void addDeck(Coordinates& coord);
			void decrementDecksNumber(){--m_decksNumber;}
			num_type getDecksNumber(){return m_decksNumber;}
			void setDecksNumber(num_type number){m_decksNumber=number;}
			deck_container& getDecks(){return m_decks;}
			void setType(GridModel::ShipType type){m_type=type;}
			GridModel::ShipType getType(){return m_type;}

		private:
			num_type m_decksNumber;
			deck_container m_decks;
			GridModel::ShipType m_type;
	};
	
	typedef std::vector<Ship> ship_container;

	
	GridModel();

	bool coordinatesAreValid(const Coordinates& coord);

	Cell& cellAt(const Coordinates& coord);
	size_t get_1deckerCount(){return m_1deckerCount;}
	size_t get_2deckerCount(){return m_2deckerCount;}
	size_t get_3deckerCount(){return m_3deckerCount;}
	size_t get_4deckerCount(){return m_4deckerCount;}
	bool shipsAreArranged()const;

	bool showSelectedShipPosition(const Coordinates& coord,ShipType type,ShipOrientation orientation);
	bool addShip(const Coordinates& coord,ShipType type,ShipOrientation orientation);
	void hitCell(Coordinates& coord);
	void reset();
	void arrangeShipsRandomly();
	coordinates_container get_notRevealedCells();
	coordinates_container get_neighboringCells(Coordinates& coord);

	void clearChanges();
	void addToChanges(Coordinates& coord);
	changes_container& get_changes();
	void set_changes(changes_container changes);
	void makeOreol(const Coordinates& coord,ShipType type,ShipOrientation orientation);

private:
	bool IsValidShipPosition(const Coordinates& coord,ShipType type,ShipOrientation orientation);
	void makeCellsHighlited(const Coordinates& coord,int quantity,ShipOrientation orientation);
	void arrangeRandomlyShipsOfType(GridModel::ShipType type);
	void pseudoRandomArrangement();
	
private:
	cell_container    m_grid;
	size_t            m_4deckerCount;
	size_t            m_3deckerCount;
	size_t            m_2deckerCount;
	size_t            m_1deckerCount;
	ship_container    m_ships;
	changes_container m_changes;
};

#endif // MODEL_H
