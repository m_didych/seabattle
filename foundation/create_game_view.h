#ifndef CREATE_GAME_VIEW_H
#define CREATE_GAME_VIEW_H


class CreateGameView : public QWidget
{
	Q_OBJECT

public:

	CreateGameView(QWidget *parent = 0);
	~CreateGameView();
	void setPort(quint16 port);
signals:
	void signalReturnToMenu();

	private slots:
 void slotQuit();

private:
	QPushButton* m_quit;
	QLabel* m_statusLabel;
	QString m_ipAddress;
	quint16 m_port;
};
#endif //CREATE_GAME_VIEW_H