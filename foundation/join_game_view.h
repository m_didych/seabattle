#ifndef JOIN_GAME_VIEW_H
#define JOIN_GAME_VIEW_H


class JoinGameView : public QWidget
{
	Q_OBJECT

public:

	JoinGameView(QWidget *parent = 0);
	~JoinGameView();


signals:
	void signalReturnToMenu();
	void signalJoinCreatedGame(QString&,int);

	private slots:
 void slotQuit();
 void slotConnectToServer();
private:
	QLineEdit*  m_IP;
	QLineEdit*  m_port;
	QPushButton* m_connect;
	QPushButton* m_quit;

};
#endif //JOIN_GAME_VIEW_H