#ifndef SERVER_H
#define SERVER_H

class Server:public QObject
{
	Q_OBJECT
public:
	Server();
	~Server();

	signals:
	void signalFinished();
	void signalError(QString err);
	void signalNewData(QString message);
	void signalDelete();

	void signalReturnToMenu();
	void signalPort(quint16);


	public slots:
	            void process();
                void slotNewConnection();
                void slotReadClient   ();
				void slotSendResponse (QString message);
				void slotError(QAbstractSocket::SocketError);
				void slotQuit();
				

private:
	void sendToClient( const QString& message);

private:
	QTcpSocket* m_tcpSocket;
	QTcpServer* m_tcpServer;
    quint16 m_nextBlockSize;
};
#endif //SERVER_H