#include "stdafx.h"

SelectModeView::SelectModeView(QWidget *parent /*= 0*/):QWidget(parent)
{
	m_createGame=new QPushButton("Create New Game");
	m_joinGame=new QPushButton("Join Created Game");
	m_singlePlayer=new QPushButton("Single Player Mode");

	connect(m_createGame,SIGNAL(clicked()),this,SLOT(slotCreateGame()));
	connect(m_joinGame,SIGNAL(clicked()),this,SLOT(slotJoinGame()));
	connect(m_singlePlayer,SIGNAL(clicked()),this,SLOT(slotSinglePlayer()));

	m_createGame->setFont(QFont("Times", 16,QFont::Bold));
	m_joinGame->setFont(QFont("Times", 16,QFont::Bold));
	m_singlePlayer->setFont(QFont("Times", 16,QFont::Bold));


	QGridLayout* layout = new QGridLayout(); 
	layout->addWidget(m_createGame,0,0);
	layout->addWidget(m_joinGame,1,0);
	layout->addWidget(m_singlePlayer,2,0);
	setLayout(layout); 
}

SelectModeView::~SelectModeView()
{
}

void SelectModeView::slotCreateGame()
{
	emit(signalCreateGame());
}

void SelectModeView::slotJoinGame()
{
	emit(signalJoinGame());
}

void SelectModeView::slotSinglePlayer()
{
	emit(signalSinglePlayer());
}