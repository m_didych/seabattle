#include "stdafx.h"

Controller::Controller(GridModel* playerGrid,GridModel* enemyGrid,QObject* parent):
	                                                             m_enemyShipsArranged(false),
																 m_playerShipsArranged(false),
																 m_enemyGrid(enemyGrid),
																 m_playerGrid(playerGrid),
																 m_isMyMove(true)
{

}

Controller::~Controller()
{

}

bool Controller::playersReady()
{
	return m_playerShipsArranged && m_enemyShipsArranged;
}

void Controller::slotSetMove(bool move)
{
	m_isMyMove=move;
	emit(signalShowIsMyMove(m_isMyMove));
}

bool Controller::interpretData(QString& message)
{

	bool result=false;
	if(m_playerShipsArranged==false && message=="as")
	{
		emit(signalArrangeShips());
		result=true;
	}
	if(m_enemyShipsArranged==false && message=="sa")
	{
		m_enemyShipsArranged=true;
	
		if(m_playerShipsArranged==false)
		{
			emit(signalSendToEnemy("wo"));
		}
		else
		{
			
			emit(signalStartGame());
		}
		result=true;
	}
	if(m_playerShipsArranged==true && message=="wo")
	{
		emit(signalEnemyShipsNotArranged());
		result=true;
	}
	if(message[0]=='m')
	{
		if(playersReady())
		{
			CellWasHit(message[1].digitValue(),message[2].digitValue());
		}
		
		result=true;
	}
	if(playersReady() && message[0]=='r')
	{
		if(message[1]=='o')
		{
			ResultOccupied(message[2].digitValue(),message[3].digitValue());
			result=true;
		}
		if(message[1]=='e')
		{
			ResultEmpty(message[2].digitValue(),message[3].digitValue());
			result=true;
		}
		if(message[1]=='d')
		{
			ResultShipSunk(message);
			result=true;
		}
	}
	if(playersReady() && message=="w")
	{

		emit(signalYouWon(true));
		result=true;
	}
	return result;
}

void Controller::slotInterpretData(QString message)
{
	if(!interpretData(message))
	{

		QString err="Unexpected error!!";
		emit(signalInvalidData(err));
	}
}

//when you click ready
void Controller::slotPlayerReady()
{
	m_playerShipsArranged=true;
	emit(signalSendToEnemy("sa"));
	if(!m_enemyShipsArranged)
	{
	 emit(signalEnemyShipsNotArranged());
	}
	else
	{
		emit(signalStartGame());
	}
}

void Controller::CellWasHit(int letter,int number)
{
	Coordinates coord(letter,number);
	m_playerGrid->hitCell(coord);
	QString message;
	if(m_playerGrid->cellAt(coord).isEmpty())
	{
		message=std::string("re"+std::to_string(coord.get_letter())+std::to_string(coord.get_number())).data();
		emit(signalSendToEnemy(message));
		m_isMyMove=true;
		emit(signalShowIsMyMove(true));
	}
	else
	{
		if(!m_playerGrid->cellAt(coord).isSunk())
		{

			message=std::string("ro"+std::to_string(coord.get_letter())+std::to_string(coord.get_number())).data();
			emit(signalSendToEnemy(message));
		}
		else
		{

			std::string temporaryString("rd");
			GridModel::changes_container::iterator iter=m_playerGrid->get_changes().begin();
			GridModel::changes_container::iterator end=m_playerGrid->get_changes().end();

			for(;iter!=end;++iter)
			{
				temporaryString+=std::to_string(iter->get_letter());
				temporaryString+=std::to_string(iter->get_number());
			}
			
			message=temporaryString.data();
			emit(signalSendToEnemy(message));
		}
		if( m_playerGrid->get_1deckerCount()+m_playerGrid->get_2deckerCount()+
				m_playerGrid->get_3deckerCount()+m_playerGrid->get_4deckerCount()==0 )
			{
				emit(signalSendToEnemy("w"));
				emit(signalYouWon(false));
			}
		m_isMyMove=false;
	}
	emit(signalDisplayPlayerGridChanges());
}

void Controller::ResultOccupied(int letter,int number)
{
	m_enemyGrid->cellAt(Coordinates(letter,number)).makeOccupied();
	m_enemyGrid->hitCell(Coordinates(letter,number));
	emit(signalDisplayEnemyGridChanges());
	m_isMyMove=true; //this is my move because enemy cell was occupied
	emit(signalShowIsMyMove(m_isMyMove));
}

void Controller::ResultEmpty(int letter,int number)
{
	m_enemyGrid->cellAt(Coordinates(letter,number)).makeEmpty();
	m_enemyGrid->hitCell(Coordinates(letter,number));
	emit(signalDisplayEnemyGridChanges());
	m_isMyMove=false;
	emit(signalShowIsMyMove(m_isMyMove));
}

void Controller::ResultShipSunk(QString& str)
{
	QString::iterator iter=str.begin()+2;
	QString::iterator end=str.end();
	Coordinates coord;
	while(iter!=end)
	{
		coord.set_letter(iter->digitValue());
		++iter;
		coord.set_number(iter->digitValue());
		++iter;
		m_enemyGrid->cellAt(coord).reveal();
		m_enemyGrid->cellAt(coord).makeOccupied();
		m_enemyGrid->cellAt(coord).hit();
		m_enemyGrid->cellAt(coord).sink();
		emit(signalDisplayEnemyCell(coord.get_letter(),coord.get_number()));
	}
		m_isMyMove=true; //this is my move because enemy ship sunk
		emit(signalShowIsMyMove(m_isMyMove));
}

void Controller::slotHitEnemyCell(int number,int letter)
{
	Coordinates coord(letter,number);
	if(m_isMyMove && !m_enemyGrid->cellAt(coord).isHit())
	{
		QString message=std::string("m"+std::to_string(letter)+std::to_string(number)).data();
		emit(signalSendToEnemy(message));
		m_isMyMove=false;
	}
}

void Controller::reset()
{
	m_enemyShipsArranged=false;
	m_playerShipsArranged=false;
	m_enemyGrid->reset();
	m_playerGrid->reset();
}
