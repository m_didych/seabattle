#include <vector>
#include <map>
#include<string>
#include <list>
#include <set>
#include <time.h>

#include <QtWidgets>
#include <QtGui>
#include <qthread.h>
#include <QtNetwork>
#include <QString>

#include "arrange_ship_view.h"
#include "battle_view.h"
#include "select_mode_view.h"
#include "create_game_view.h"
#include "server.h"
#include "model.h"
#include "sbwindow.h"
#include "enemy_ships_not_arranged_view.h"
#include "client.h"
#include "join_game_view.h"
#include "controller.h"
#include "computer.h"
