#ifndef ARRANGE_SHIP_VIEW_H
#define ARRANGE_SHIP_VIEW_H


#include "model.h"

class ArrangeShipView : public QWidget
{
	Q_OBJECT

public:
	typedef std::map<std::string,QBrush> BrushColection;

	ArrangeShipView(GridModel* grid,QWidget *parent = 0);
	~ArrangeShipView();

	void reset(){slotReset();}
	void displayCell(const Coordinates& coord);
	void displayGrid();
	void displayGridChanges();

signals:
	void signalReady();
	void signalReturnToMenu();


	private slots:

		void slotSelectShipType1();
		void slotSelectShipType2();
		void slotSelectShipType3();
		void slotSelectShipType4();

		void slotDisplayFutureShip(int number,int letter);
		void slotChangeOrientation();
		void slotAddShip(int number,int letter);
		void slotReset();
		void slotReady();
		void slotQuit();
		void slotRandom();

private:
	void SelectShipType(GridModel::ShipType type);

private:
	QTableWidget*               m_gridView;

	QPushButton*                m_1decker;
	QPushButton*                m_2decker;
	QPushButton*                m_3decker;
	QPushButton*                m_4decker;
	QPushButton*                m_changeOrientation;
	QPushButton*                m_ready;
	QPushButton*                m_reset;
	QPushButton*                m_quit;
	QPushButton*                m_arrangeRandomly;

	GridModel*                  m_grid;

	BrushColection              m_brushes;

	GridModel::ShipType         m_shipType;
	bool                        m_isShipTypeSelected;
	bool                        m_higlitedValid;
	GridModel::ShipOrientation  m_orientation;
};

#endif // VIEW_H
