#ifndef SBWINDOW_H
#define SBWINDOW_H

class EnemyShipsNotArrangedView;
class JoinGameView;
class Controller;

class SBWindow : public QMainWindow
{
	Q_OBJECT

public:
	SBWindow(GridModel* playerGrid,GridModel* enemyGrid,QWidget *parent = 0);
	~SBWindow();

signals:
	void signalSetMove(bool);
	void signalSetInfoText(QString);

	private slots:
		void slotSetBattleView();
		void slotSetArrangeShipView();
		void slotSetJoinGameView();
		void slotSetSelectModeView();
		void slotCreateGame();
		void slotGameOver(bool);
		void slotEnemyShipsNotArranged();
		void slotJoinCreatedGame(QString&,int);
		void slotSinglePlayer();
		void slotPort(quint16);
		void slotError(QString);

private:
	QStackedWidget* m_view;
	SelectModeView* m_selectModeView;
	ArrangeShipView* m_arrangeShipView;
	BattleView* m_battleView;
	CreateGameView* m_createGameView;
	EnemyShipsNotArrangedView* m_enemyShipsNotArrangedView;
	JoinGameView* m_joinGameView;


	GridModel* m_playerGrid;
	GridModel* m_enemyGrid;
	Controller* m_controller;

};

#endif // SBWINDOW_H
