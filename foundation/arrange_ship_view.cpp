#include "stdafx.h"

ArrangeShipView::ArrangeShipView(GridModel* grid,QWidget *parent)
	: m_grid(grid),QWidget(parent)
{
	int size=m_grid->size;
	m_higlitedValid=true;
	m_shipType=GridModel::ShipType_FourDecker;
	m_orientation=GridModel::ShipOrientation_Horisontal;

	m_gridView=new QTableWidget(size,size,this);
	for(int i=0;i!=size;++i)
	{
		m_gridView->setColumnWidth(i,20);
		m_gridView->setRowHeight(i,20);
		m_gridView->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
		m_gridView->verticalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
	}

	m_gridView->setGeometry(0,0,225,225);
	m_gridView->setMaximumSize(225,225);
	m_gridView->setMinimumSize(225,225);

	m_gridView->setHorizontalHeaderLabels(QString("A;B;C;D;E;F;G;H;I;L").split(";"));
	m_gridView->horizontalHeader()->setSectionsClickable(false);
	m_gridView->verticalHeader()->setSectionsClickable(false);


	//loading brushes
	QBrush* brush = new QBrush();
	QPixmap pixmap;

	pixmap.load("pictures/not_revealed.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("not_revealed",(*brush)));

	pixmap.load("pictures/empty.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("empty",(*brush)));

	pixmap.load("pictures/not_empty.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("not_empty",(*brush)));

	pixmap.load("pictures/hit.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("hit",(*brush)));

	pixmap.load("pictures/sunk.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("sunk",(*brush)));

	pixmap.load("pictures/highlited_valid.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("highlited_valid",(*brush)));

	pixmap.load("pictures/highlited_invalid.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("highlited_invalid",(*brush)));
	///////////////////////////////////

	m_1decker = new QPushButton("&Single-deckers (4)");
	m_2decker = new QPushButton("&Two-deckers (3)");
	m_3decker = new QPushButton("&Three-deckers (2)");
	m_4decker = new QPushButton("&Four-deckers (1)");
	m_ready= new QPushButton("&Ready");
	m_reset= new QPushButton("&Reset");
	m_changeOrientation = new QPushButton("&Change ship orientation");
	m_quit=new QPushButton("&Quit");
	m_arrangeRandomly=new QPushButton("&Arrange ships randomly");

	m_ready->setDisabled(true);



	connect(m_1decker,SIGNAL(clicked()),this,SLOT(slotSelectShipType1()));
	connect(m_2decker,SIGNAL(clicked()),this,SLOT(slotSelectShipType2()));
	connect(m_3decker,SIGNAL(clicked()),this,SLOT(slotSelectShipType3()));
	connect(m_4decker,SIGNAL(clicked()),this,SLOT(slotSelectShipType4()));
	connect(m_changeOrientation,SIGNAL(clicked()),this,SLOT(slotChangeOrientation()));

	connect(m_gridView,SIGNAL(cellEntered(int,int)),this,SLOT(slotDisplayFutureShip(int,int)));
	connect(m_gridView,SIGNAL(cellClicked(int,int)),this,SLOT(slotAddShip(int,int)));

	connect(m_reset,SIGNAL(clicked()),this,SLOT(slotReset()));
	connect(m_ready,SIGNAL(clicked()),this,SLOT(slotReady()));
	connect(m_quit,SIGNAL(clicked()),this,SLOT(slotQuit()));
	connect(m_arrangeRandomly,SIGNAL(clicked()),this,SLOT(slotRandom()));

	for(int i=0;i!=size;++i)
	{
		for(int j=0;j!=size;++j)
		{
			QTableWidgetItem *item = new QTableWidgetItem();
			m_gridView->setItem(i,j,item);
			m_gridView->item(i,j)->setFlags(Qt::ItemIsDragEnabled & Qt::ItemIsUserCheckable & Qt::ItemIsSelectable);
		}
	}

	QGridLayout* layout = new QGridLayout(); 
	 
	layout->addWidget(m_gridView,0,0,9,1);
	layout->addWidget(m_1decker,1,1);
	layout->addWidget(m_2decker,1,2);
	layout->addWidget(m_3decker,2,1);
	layout->addWidget(m_4decker,2,2);
	layout->addWidget(m_changeOrientation,3,1,1,2);
	layout->addWidget(m_arrangeRandomly,5,1,1,2);
	layout->addWidget(m_reset,6,1,1,2);
	layout->addWidget(m_ready,8,1);
	layout->addWidget(m_quit,8,2);

	setLayout(layout); 
	displayGrid();

}

ArrangeShipView::~ArrangeShipView()
{

}


void ArrangeShipView::SelectShipType(GridModel::ShipType type)
{
	m_shipType=type;
	m_isShipTypeSelected=true;
	m_gridView->setMouseTracking(true);
}
void ArrangeShipView::slotSelectShipType1()
{
	SelectShipType(GridModel::ShipType_SingleDecker);
}
void ArrangeShipView::slotSelectShipType2()
{
	SelectShipType(GridModel::ShipType_TwoDecker);
}
void ArrangeShipView::slotSelectShipType3()
{
	SelectShipType(GridModel::ShipType_ThreeDecker);
}
void ArrangeShipView::slotSelectShipType4()
{
	SelectShipType(GridModel::ShipType_FourDecker);
}

void ArrangeShipView::displayCell(const Coordinates& coord)
{
	QBrush* brush = new QBrush();
	Cell cell=m_grid->cellAt(coord);
	if(!cell.isRevealed() || (cell.isEmpty() && !cell.isHit()) )
	{
		(*brush)=m_brushes["not_revealed"];
	}
	else
	{
		if(cell.isEmpty() && cell.isHit())
		{
			(*brush)=m_brushes["empty"];
		}
		if(!cell.isEmpty() && !cell.isHit() && !cell.isSunk())
		{
			(*brush)=m_brushes["not_empty"];
		}
		if(!cell.isEmpty() && cell.isHit() && !cell.isSunk())
		{
			(*brush)=m_brushes["hit"];
		}
		if(!cell.isEmpty() && cell.isSunk())
		{
			(*brush)=m_brushes["sunk"];
		}
	}

	if(cell.isHighlited() && m_higlitedValid)
	{
		(*brush)=m_brushes["highlited_valid"];
	}

	if(cell.isHighlited() && !m_higlitedValid)
	{
		(*brush)=m_brushes["highlited_invalid"];
	}

	m_gridView->item(coord.get_number(),coord.get_letter())->setBackground(*brush);
}

void ArrangeShipView::displayGrid()
{
	Coordinates coord(0,0);
	for(Coordinates::num_type i=0;i!=m_grid->size;++i)
	{
		coord.set_number(i);

		for(Coordinates::num_type j=0;j!=m_grid->size;++j)
		{
			coord.set_letter(j);
			displayCell(coord);
		}
	}
}

void ArrangeShipView::displayGridChanges()
{
	GridModel::changes_container changes=m_grid->get_changes();
	GridModel::changes_container temporaryChanges;
	GridModel::changes_container::iterator iter=changes.begin();
	GridModel::changes_container::iterator end=changes.end();
	for(;iter!=end;++iter)
	{
		displayCell(*iter);
		if(m_grid->cellAt(*iter).isHighlited())
		{
			m_grid->cellAt(*iter).makeNotHighlited();
			temporaryChanges.insert(*iter);
		}
	}
	m_grid->clearChanges();
	m_grid->set_changes(temporaryChanges);
}

void ArrangeShipView::slotDisplayFutureShip(int number,int letter)
{
	if(m_gridView->hasMouseTracking())
	{
		Coordinates coord(letter,number);
		m_higlitedValid=m_grid->showSelectedShipPosition(coord,m_shipType,m_orientation);
		displayGridChanges();
	}
}

void ArrangeShipView::slotChangeOrientation()
{
	if(m_orientation==GridModel::ShipOrientation_Horisontal)
	{
		m_orientation=GridModel::ShipOrientation_Vertical;
	}
	else
	{
		m_orientation=GridModel::ShipOrientation_Horisontal;
	}
}

void ArrangeShipView::slotAddShip(int number,int letter)
{
	std::string str;
	int count=0;

	if(m_gridView->hasMouseTracking() && m_grid->addShip(Coordinates(letter,number),m_shipType,m_orientation))
	{
		switch (m_shipType)
			{
			case GridModel::ShipType_SingleDecker:
				{
						if(m_grid->get_1deckerCount()==4)
					{
						m_1decker->setDisabled(true);
					}

					count=4-m_grid->get_1deckerCount();
					str="Single-deckers ("+std::to_string(count)+')';
					QString qstr(str.data());
					m_1decker->setText(qstr);
				}
				break;
			case GridModel::ShipType_TwoDecker:
				{
					if(m_grid->get_2deckerCount()==3)
					{
						m_2decker->setDisabled(true);
					}
					count=3-m_grid->get_2deckerCount();
					str="Two-deckers ("+std::to_string(count)+')';
					QString qstr(str.data());
					m_2decker->setText(qstr);
				}
				break;
			case GridModel::ShipType_ThreeDecker:
				{
					if(m_grid->get_3deckerCount()==2)
					{
						m_3decker->setDisabled(true);
					}
					count=2-m_grid->get_3deckerCount();
					str="Three-deckers ("+std::to_string(count)+')';
					QString qstr(str.data());
					m_3decker->setText(qstr);
				}
				break;
			case GridModel::ShipType_FourDecker:
				{
					if(m_grid->get_4deckerCount()==1)
					{
						m_4decker->setDisabled(true);
					}
					count=1-m_grid->get_1deckerCount();
					str="Four-deckers ("+std::to_string(count)+')';
					QString qstr(str.data());
					m_4decker->setText(qstr);
				}
				break;
			default:
				break;
			}
		if(m_grid->shipsAreArranged())
		{
			m_ready->setDisabled(false);
		}
		m_gridView->setMouseTracking(false);
		displayGridChanges();
	}
}

void ArrangeShipView::slotReset()
{
	setMouseTracking(false);

	m_grid->reset();

	m_1decker->setDisabled(false);
	m_1decker->setText("&Single-deckers (4)");

	m_2decker->setDisabled(false);
	m_2decker->setText("&Two-deckers (3)");

	m_3decker->setDisabled(false);
	m_3decker->setText("&Three-deckers (2)");

	m_4decker->setDisabled(false);
	m_4decker->setText("&Four-deckers (1)");

	m_ready->setDisabled(true);

	displayGrid();
}

void ArrangeShipView::slotReady()
{
	emit(signalReady());
}

void ArrangeShipView::slotQuit()
{
	emit(signalReturnToMenu());
}

void ArrangeShipView::slotRandom()
{
	slotReset();
	m_grid->arrangeShipsRandomly();
	displayGrid();

	m_1decker->setDisabled(true);
	m_1decker->setText("&Single-deckers (0)");

	m_2decker->setDisabled(true);
	m_2decker->setText("&Two-deckers (0)");

	m_3decker->setDisabled(true);
	m_3decker->setText("&Three-deckers (0)");

	m_4decker->setDisabled(true);
	m_4decker->setText("&Four-deckers (0)");

	m_ready->setDisabled(false);

}