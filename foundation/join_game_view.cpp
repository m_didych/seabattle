#include "stdafx.h"

JoinGameView::JoinGameView(QWidget *parent /*=0*/)
{
	m_IP = new QLineEdit;
	m_port = new QLineEdit;
	m_connect = new QPushButton("&Connect");
	m_quit = new QPushButton("&Quit");


	m_IP->setText("localhost");//default host
	m_port->setText("");

	connect(m_connect, SIGNAL(clicked()),this, SLOT(slotConnectToServer()));
	connect(m_quit, SIGNAL(clicked()),this, SLOT(slotQuit()));

    //Layout setup
    QGridLayout* layout=new QGridLayout;   
    layout->addWidget(new QLabel("<H1>Join created game</H1>"),1,1,2,2);
	layout->addWidget(new QLabel("<H3>Enter IP:</H3>"),3,0,2,1);
	layout->addWidget(m_IP,3,1,2,2);
	layout->addWidget(new QLabel("<H3>Enter Port:</H3>"),4,0,2,1);
	layout->addWidget(m_port,4,1,2,2);
    layout->addWidget(m_connect,7,2);
	layout->addWidget(m_quit,7,3);
    setLayout(layout);
}

JoinGameView::~JoinGameView()
{
}

void JoinGameView::slotQuit()
{
	emit(signalReturnToMenu());
}

void JoinGameView::slotConnectToServer()
{
	
	emit(signalJoinCreatedGame(m_IP->text(),m_port->text().toInt()));
}