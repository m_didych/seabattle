#include "stdafx.h"

Client::Client(QString& IP,int port,QObject* parent /*=0*/) : QObject(parent), 
	                                                          m_nextBlockSize(0),
															  m_IP(IP),
															  m_port(port)
{
	connect(this, SIGNAL(signalFinished()), this, SLOT(deleteLater()));
}

void Client::process()
{
	m_tcpSocket = new QTcpSocket(this);
    connect(m_tcpSocket, SIGNAL(readyRead()), SLOT(slotReadyRead()));
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,         SLOT(slotError(QAbstractSocket::SocketError))
           );
	m_tcpSocket->connectToHost(m_IP, m_port);
}


void Client::slotReadyRead()
{
    QDataStream in(m_tcpSocket);
    in.setVersion(QDataStream::Qt_5_0);
    for (;;) {
        if (!m_nextBlockSize) {
            if (m_tcpSocket->bytesAvailable() < sizeof(quint16)) {
                break;
            }
            in >> m_nextBlockSize;
        }

        if (m_tcpSocket->bytesAvailable() < m_nextBlockSize) {
            break;
        }
        QString message;
        in  >> message;
		emit(signalNewData(message));
        m_nextBlockSize = 0;
    }
	
}

void Client::slotError(QAbstractSocket::SocketError err)
{
	
    QString strError = 
					(err == QAbstractSocket::HostNotFoundError ? 
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ? 
					"Your opponent disconnected":
					err == QAbstractSocket::ConnectionRefusedError ? 
					"The connection was refused." :
					QString(m_tcpSocket->errorString())
				);	
	emit(signalError(strError));
	slotQuit();
}

void Client::sendToServer(QString message)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_0);
    out << quint16(0) << message;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

    m_tcpSocket->write(arrBlock);
}

void Client::slotSendResponse(QString message)
{
	sendToServer( message);
}

void Client::slotQuit()
{
	if(m_tcpSocket->state()!=QAbstractSocket::UnconnectedState)
	{
		m_tcpSocket->close();
	}
	emit(signalFinished());
}







