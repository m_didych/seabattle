#include "stdafx.h"


Server::Server():m_nextBlockSize(0)
{
	connect(this, SIGNAL(signalDelete()), this, SLOT(deleteLater()));
}

Server::~Server()
{

	delete m_tcpServer;

	emit(signalFinished());
}

void Server::process()
{
	m_tcpServer = new QTcpServer(this);

	 if (!m_tcpServer->listen(QHostAddress::Any)) 
	{
		emit(signalError("Unable to start the server"));
    }
	 emit(signalPort(m_tcpServer->serverPort()));
	 connect(m_tcpServer, SIGNAL(newConnection()), this,SLOT(slotNewConnection()));
}

void Server::slotNewConnection()
{
   m_tcpSocket = m_tcpServer->nextPendingConnection();
    connect(m_tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this,         SLOT(slotError(QAbstractSocket::SocketError))
           );
    connect(m_tcpSocket, SIGNAL(readyRead()), 
            this,          SLOT(slotReadClient())
           );

    sendToClient("as");
	emit(signalNewData("as"));
}

void Server::slotReadClient()
{
    QTcpSocket* pClientSocket = (QTcpSocket*)sender();
    QDataStream in(pClientSocket);
    in.setVersion(QDataStream::Qt_5_0);
    for (;;) {
        if (!m_nextBlockSize) {
            if (pClientSocket->bytesAvailable() < sizeof(quint16)) {
                break;
            }
            in >> m_nextBlockSize;
        }

        if (pClientSocket->bytesAvailable() < m_nextBlockSize) 
		{
            break;
        }
        QString message;
        in >> message;

        m_nextBlockSize = 0;

		emit(signalNewData(message));
	}
}

void Server::sendToClient(const QString& message)
{
    QByteArray  arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_5_0);
    out << quint16(0) << message;

    out.device()->seek(0);
    out << quint16(arrBlock.size() - sizeof(quint16));

	m_tcpSocket->write(arrBlock);
}

void Server::slotSendResponse(QString message)
{
	sendToClient(message);
}

void Server::slotQuit()
{
	if(m_tcpSocket->state()!=QAbstractSocket::UnconnectedState)
	{
		m_tcpSocket->close();
	}
	m_tcpServer->close();
	emit(signalDelete());
}

void Server::slotError(QAbstractSocket::SocketError err)
{
	
    QString strError = 
					(err == QAbstractSocket::HostNotFoundError ? 
					"The host was not found." :
					err == QAbstractSocket::RemoteHostClosedError ? 
					"Your opponent disconnected":
					err == QAbstractSocket::ConnectionRefusedError ? 
					"The connection was refused." :
					QString(m_tcpSocket->errorString())
				);	
	emit(signalError(strError));
	slotQuit();
}

