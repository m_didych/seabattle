#include "stdafx.h"

CreateGameView::CreateGameView(QWidget *parent /*=0*/):m_port(0)
{
     QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
     // use the first non-localhost IPv4 address
     for (int i = 0; i < ipAddressesList.size(); ++i) {
         if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
             ipAddressesList.at(i).toIPv4Address()) {
             m_ipAddress = ipAddressesList.at(i).toString();
             break;
         }
     }
     // if we did not find one, use IPv4 localhost
     if (m_ipAddress.isEmpty())
         m_ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    
	 m_statusLabel=new QLabel ;
	 m_statusLabel->setText(tr("<H3> IP: %1</H3>\n<H3>port: %2 </H3>").arg(m_ipAddress).arg(m_port));
	
	 m_quit=new QPushButton("&Quit");

	 connect(m_quit,SIGNAL(clicked()),this,SLOT(slotQuit()));

	//Layout setup
    QGridLayout* layout = new QGridLayout; 
    layout->addWidget(new QLabel("<H1> Waiting for opponent </H1>"),1,1,2,2);
	layout->addWidget(m_statusLabel,3,0,2,2);
	layout->addWidget(m_quit,6,3);
    setLayout(layout);
}

CreateGameView::~CreateGameView()
{
}

void CreateGameView::slotQuit()
{
	emit(signalReturnToMenu());
}

void CreateGameView::setPort(quint16 port)
{
	m_port=port;
	m_statusLabel->setText(tr("<H3> IP: %1</H3>\n<H3>port: %2 </H3>").arg(m_ipAddress).arg(m_port));
}