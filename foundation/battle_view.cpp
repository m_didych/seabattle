#include "stdafx.h"

BattleView::BattleView(GridModel* playerGrid,GridModel* enemyGrid,QWidget *parent)
	: m_playerGrid(playerGrid),m_enemyGrid(enemyGrid),QWidget(parent)
{
	int size=m_playerGrid->size;
	m_quit=new QPushButton("Quit",this);
	m_info=new QLabel("",this);


	m_playerGridView=new QTableWidget(size,size,this);
	for(int i=0;i!=size;++i)
	{
		m_playerGridView->setColumnWidth(i,20);
		m_playerGridView->setRowHeight(i,20);
		m_playerGridView->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
		m_playerGridView->verticalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
	}

	m_playerGridView->setMaximumSize(225,225);
	m_playerGridView->setMinimumSize(225,225);
	m_playerGridView->setHorizontalHeaderLabels(QString("A;B;C;D;E;F;G;H;I;L").split(";"));
	m_playerGridView->horizontalHeader()->setSectionsClickable(false);
	m_playerGridView->verticalHeader()->setSectionsClickable(false);

	m_enemyGridView=new QTableWidget(size,size,this);
	for(int i=0;i!=size;++i)
	{
		m_enemyGridView->setColumnWidth(i,20);
		m_enemyGridView->setRowHeight(i,20);
		m_enemyGridView->horizontalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
		m_enemyGridView->verticalHeader()->setSectionResizeMode(i,QHeaderView::Fixed);
	}

	m_enemyGridView->setMaximumSize(225,225);
	m_enemyGridView->setMinimumSize(225,225);
	m_enemyGridView->setHorizontalHeaderLabels(QString("A;B;C;D;E;F;G;H;I;L").split(";"));
	m_enemyGridView->horizontalHeader()->setSectionsClickable(false);
	m_enemyGridView->verticalHeader()->setSectionsClickable(false);

	connect(m_enemyGridView,SIGNAL(cellClicked(int,int)),this,SLOT(slotCellClicked(int,int)));
	connect(m_quit,SIGNAL(clicked()),this,SLOT(slotQuit()));

	//loading brushes
	QBrush* brush = new QBrush();
	QPixmap pixmap;

	pixmap.load("pictures/not_revealed.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("not_revealed",(*brush)));

	pixmap.load("pictures/empty.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("empty",(*brush)));

	pixmap.load("pictures/not_empty.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("not_empty",(*brush)));

	pixmap.load("pictures/hit.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("hit",(*brush)));

	pixmap.load("pictures/sunk.png");
	brush->setTexture(pixmap);
	m_brushes.insert(std::make_pair("sunk",(*brush)));

	///////////////////////////////////


	for(int i=0;i!=size;++i)
	{
		for(int j=0;j!=size;++j)
		{
			QTableWidgetItem *enemyitem = new QTableWidgetItem();
			m_enemyGridView->setItem(i,j,enemyitem);
			m_enemyGridView->item(i,j)->setFlags(0);
			QTableWidgetItem *playeritem = new QTableWidgetItem();
			m_playerGridView->setItem(i,j,playeritem);
			m_playerGridView->item(i,j)->setFlags(0);
		}
	}
	

	QGridLayout* layout = new QGridLayout(); 
	 
	layout->addWidget(new QLabel("Your opponent's grid"),0,0);
	layout->addWidget(new QLabel("Your grid"),0,2);
	layout->addWidget(m_enemyGridView,1,0,7,2);
	layout->addWidget(m_playerGridView,1,2,7,2);
	layout->addWidget(m_info,8,0);
	layout->addWidget(m_quit,8,3);
	setLayout(layout); 

	displayGrid(m_enemyGrid,m_enemyGridView);
	displayGrid(m_playerGrid,m_playerGridView);

}

BattleView::~BattleView()
{
}


void BattleView::slotCellClicked(int number,int letter)
{
	emit(signalCellClicked(number,letter));
}

void BattleView::displayCell(GridModel* grid, QTableWidget* gridView,const Coordinates& coord)
{
	QBrush* brush = new QBrush();
	Cell cell=grid->cellAt(coord);
	if(!cell.isRevealed() || (cell.isEmpty() && !cell.isHit()) )
	{
		(*brush)=m_brushes["not_revealed"];
	}
	else
	{
		if(cell.isEmpty() && cell.isHit())
		{
			(*brush)=m_brushes["empty"];
		}
		if(!cell.isEmpty() && !cell.isHit() && !cell.isSunk())
		{
			(*brush)=m_brushes["not_empty"];
		}
		if(!cell.isEmpty() && cell.isHit() && !cell.isSunk())
		{
			(*brush)=m_brushes["hit"];
		}
		if(!cell.isEmpty() && cell.isSunk())
		{
			(*brush)=m_brushes["sunk"];
		}
	}

	gridView->item(coord.get_number(),coord.get_letter())->setBackground(*brush);
}

void BattleView::displayGrid(GridModel* grid, QTableWidget* gridView)
{
	Coordinates coord(0,0);
	for(Coordinates::num_type i=0;i!=grid->size;++i)
	{
		coord.set_number(i);

		for(Coordinates::num_type j=0;j!=grid->size;++j)
		{
			coord.set_letter(j);
			displayCell(grid,gridView,coord);
		}
	}
	grid->clearChanges();
}

void BattleView::displayGridChanges(GridModel* grid, QTableWidget* gridView)
{
	GridModel::changes_container changes=grid->get_changes();
	GridModel::changes_container temporaryChanges;
	GridModel::changes_container::iterator iter=changes.begin();
	GridModel::changes_container::iterator end=changes.end();
	for(;iter!=end;++iter)
	{
		displayCell(grid,gridView,*iter);
		if(grid->cellAt(*iter).isHighlited())
		{
			grid->cellAt(*iter).makeNotHighlited();
			temporaryChanges.insert(*iter);
		}
	}
	grid->clearChanges();
	grid->set_changes(temporaryChanges);
}

void BattleView::displayPlayerGrid()
{
	displayGrid(m_playerGrid,m_playerGridView);
}
void BattleView::displayEnemyGrid()
{
	displayGrid(m_enemyGrid,m_enemyGridView);
}

void BattleView::slotDisplayEnemyGridChanges()
{
	displayGridChanges(m_enemyGrid, m_enemyGridView);
}
void BattleView::slotDisplayPlayerGridChanges()
{
	displayGridChanges(m_playerGrid, m_playerGridView);
}

void BattleView::slotDisplayEnemyCell(int letter,int number)
{
	Coordinates coord(letter,number);
	displayCell(m_enemyGrid, m_enemyGridView,coord);
}

void BattleView::SlotShowWhoseMove(bool isMyMove)
{
	if(isMyMove)
	{
		m_info->setText("Your move!");
	}
	else
	{
		m_info->setText("Your opponent's move!");
	}
}
void BattleView::slotQuit()
{
	emit(signalReturnToMenu());
}
