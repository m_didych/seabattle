#ifndef CONTROLLER_H
#define CONTROLLER_H

class Controller:public QObject
{
	Q_OBJECT
public:
	Controller(GridModel* playerGrid,GridModel* enemyGrid,QObject* parent);
	~Controller();

	void reset();

	public slots:
		void slotInterpretData(QString message);
		void slotPlayerReady();
		void slotHitEnemyCell(int number,int letter);

		void slotSetMove(bool move);


signals:
		void signalInvalidData(QString);
		void signalSendToEnemy(QString);

		void signalStartGame();
		void signalArrangeShips();
		void signalEnemyShipsNotArranged();

		void signalYouWon(bool);

		void signalShowIsMyMove(bool);
		void signalDisplayEnemyGridChanges();
		void signalDisplayPlayerGridChanges();
		void signalDisplayEnemyCell(int letter,int number);

private:
	bool interpretData(QString& message);
	bool playersReady();

	void CellWasHit(int letter,int number);
	void ResultOccupied(int letter,int number);
	void ResultEmpty(int letter,int number);
	void ResultShipSunk(QString& message);

private:
	GridModel* m_playerGrid;
	GridModel* m_enemyGrid;
	bool m_playerShipsArranged;
	bool m_enemyShipsArranged;
	bool m_isMyMove;
};

#endif //CONTROLLER_H