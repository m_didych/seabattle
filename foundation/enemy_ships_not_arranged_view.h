#ifndef ENEMY_SHIPS_NOT_ARRANGED_VIEW_H
#define ENEMY_SHIPS_NOT_ARRANGED_VIEW_H


class EnemyShipsNotArrangedView : public QWidget
{
	Q_OBJECT

public:

	EnemyShipsNotArrangedView(QWidget *parent = 0);
	~EnemyShipsNotArrangedView();

signals:
	void signalReturnToMenu();

	private slots:
 void slotQuit();
private:

private:
	QPushButton* m_quit;
};

#endif //CREATE_GAME_VIEW_H