#ifndef COMPUTER_H
#define COMPUTER_H

class Computer: public QObject
{
	Q_OBJECT
	public:
		Computer();
		~Computer();

		public slots:
			void process();
			void slotInterpretData(QString message);
			void slotMakeMove();
	signals:
		void signalSendToPlayer(QString);
		void signalStartGame();
		void signalInvalidData(QString);

		void signalMakeMove();

		void signalFinished();

	private:
		void arrangeShips();
		void makeRandomMove();
		void tryToKillFoundShip();
		void hitCell(Coordinates& coord);
		void chooseNextCellToHit();

		bool interpretData(QString& message);
		void cellWasHit(int,int);
		void resultOccupied(int,int);
		void resultEmpty(int,int);
		void resultShipSunk(QString& str);

	private:
		GridModel* m_playerGrid;
		GridModel* m_enemyGrid;

		bool m_foundShip;
		bool m_foundOrientation;
		Coordinates m_FirstFoundShipPart;
		Coordinates m_CurrentFoundShipPart;
		int m_orientation;
		bool m_changeDirection;
		GridModel::coordinates_container m_neighbors;

};


#endif //COMPUTER_H