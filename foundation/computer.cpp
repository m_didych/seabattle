#include "stdafx.h"

Computer::Computer():
					 m_foundOrientation(false),
					 m_foundShip(false),
					 m_orientation(-1),
					 m_changeDirection(false)
{
	connect(this,SIGNAL(signalMakeMove()),this,SLOT(slotMakeMove()));
}

void Computer::process()
{

	m_enemyGrid=new GridModel;
	m_playerGrid=new GridModel;
	emit(signalSendToPlayer("as"));
	srand((unsigned)time(NULL));
	arrangeShips();
}

Computer::~Computer()
{
	delete m_enemyGrid;
	delete m_playerGrid;
	signalFinished();
}

void Computer::arrangeShips()
{
	m_playerGrid->arrangeShipsRandomly();
	m_playerGrid->clearChanges();
	emit(signalSendToPlayer("sa"));
}

void Computer::hitCell(Coordinates& coord)
{
	QString message;
	message=std::string("m"+std::to_string(coord.get_letter())+std::to_string(coord.get_number())).data();
	emit(signalSendToPlayer(message));
}

void Computer::makeRandomMove()
{
		srand((unsigned)time(NULL));

		GridModel::coordinates_container coordinates=m_enemyGrid->get_notRevealedCells();
		if(coordinates.size()!=0)
		{
			int index=rand()%coordinates.size();
			hitCell(coordinates[index]);
		}
}

void Computer::chooseNextCellToHit()
{
	bool moveWasMade=false;
    //during previous move we have not reached edge of grid,
	//empty cell that was hit befor or some other ship's oreol
	if(!m_changeDirection)
	{
		//ship's orientation is vertical
		if(m_CurrentFoundShipPart.get_letter()==m_FirstFoundShipPart.get_letter())
		{
			//we should move to cell above CurrentFoundShipPart
			if(m_CurrentFoundShipPart.get_number()<m_FirstFoundShipPart.get_number())
			{
				//we have not reached edge of grid
				if(m_CurrentFoundShipPart.get_number()!=0)
				{
					Coordinates coord(m_CurrentFoundShipPart.get_letter(),m_CurrentFoundShipPart.get_number()-1);

					//if cell above CurrentFoundShipPart is empty and was revealed before we have to change direction
					if(m_enemyGrid->cellAt(coord).isEmpty() && m_enemyGrid->cellAt(coord).isRevealed())
					{
						m_changeDirection=true;
					}

					//if cell above CurrentFoundShipPart is not revealed we check it
					else
					{
						moveWasMade=true;
						hitCell(coord);
					}
				}

				// we reached edge of grid and we have to change direction
				else
				{
					m_changeDirection=true;
				}
			}

			//we should move to cell under CurrentFoundShipPart
			if(m_CurrentFoundShipPart.get_number()>m_FirstFoundShipPart.get_number())
			{
				if(m_CurrentFoundShipPart.get_number()!=GridModel::size-1)
				{
					Coordinates coord(m_CurrentFoundShipPart.get_letter(),m_CurrentFoundShipPart.get_number()+1);
					if(m_enemyGrid->cellAt(coord).isEmpty() && m_enemyGrid->cellAt(coord).isRevealed())
					{
						m_changeDirection=true;
					}
					else
					{
						moveWasMade=true;
						hitCell(coord);
					}
				}
				else
				{
					m_changeDirection=true;
				}
			}
		}

		//ship's orientation is horisontal
		else
		{
			if(m_CurrentFoundShipPart.get_letter()<m_FirstFoundShipPart.get_letter())
			{
				if(m_CurrentFoundShipPart.get_letter()!=0)
				{
					Coordinates coord(m_CurrentFoundShipPart.get_letter()-1,m_CurrentFoundShipPart.get_number());
					if(m_enemyGrid->cellAt(coord).isEmpty() && m_enemyGrid->cellAt(coord).isRevealed())
					{
						m_changeDirection=true;
					}
					else
					{
						moveWasMade=true;
						hitCell(coord);
					}
				}
				else
				{
					m_changeDirection=true;
				}
			}
			if(m_CurrentFoundShipPart.get_letter()>m_FirstFoundShipPart.get_letter())
			{
				if(m_CurrentFoundShipPart.get_letter()!=GridModel::size-1)
				{
					Coordinates coord(m_CurrentFoundShipPart.get_letter()+1,m_CurrentFoundShipPart.get_number());
					if(m_enemyGrid->cellAt(coord).isEmpty() && m_enemyGrid->cellAt(coord).isRevealed())
					{
						m_changeDirection=true;
					}
					else
					{
						moveWasMade=true;
						hitCell(coord);
					}
				}
				else
				{
					m_changeDirection=true;
				}
			}
		}
	}

	//we know that we have to change direction
	if(m_changeDirection && !moveWasMade)
	{
		int possibleOrientation=m_orientation;
		do
		{
			possibleOrientation=(++possibleOrientation)%m_neighbors.size();
		}
		while(!(m_neighbors[possibleOrientation].get_letter()==m_neighbors[m_orientation].get_letter() ||
			   m_neighbors[possibleOrientation].get_number()==m_neighbors[m_orientation].get_number()));
		//we start moving from first found ship deck in opposite direction 
			   m_orientation=possibleOrientation;
			   hitCell(m_neighbors[m_orientation]);
	}
}

void Computer::tryToKillFoundShip()
{
	if(m_foundOrientation)//we know orientation and atleast two ship decks
	{
		chooseNextCellToHit();
	}
	else
	{
		//we try to find orientation. In order to do that we look for neighbor not revealed cells
		//if we find another ship deck we can determine orientation
			m_orientation=(++m_orientation)%m_neighbors.size();
		while(m_enemyGrid->cellAt(m_neighbors[m_orientation]).isRevealed() &&
			   m_enemyGrid->cellAt(m_neighbors[m_orientation]).isEmpty())
		{
			m_orientation=(++m_orientation)%m_neighbors.size();
		}
		if(m_orientation<m_neighbors.size())
		{
			hitCell(m_neighbors[m_orientation]);
		}
	}
}

bool Computer::interpretData(QString& message)
{

	bool result=false;
	if(message=="sa")
	{
		emit(signalStartGame());
		result=true;
	}
	if(message[0]=='m')
	{
		cellWasHit(message[1].digitValue(),message[2].digitValue());
		result=true;
	}
	if(message[0]=='r')
	{
		if(message[1]=='o')
		{
			resultOccupied(message[2].digitValue(),message[3].digitValue());
			result=true;
		}
		if(message[1]=='e')
		{
			resultEmpty(message[2].digitValue(),message[3].digitValue());
			result=true;
		}
		if(message[1]=='d')
		{
			resultShipSunk(message);
			result=true;
		}
	}
	if(message=="w")
	{
		result=true;
	}
	return result;
}

void Computer::slotInterpretData(QString message)
{
	if(!interpretData(message))
	{
		QString err="Unexpected error!";
		emit(signalInvalidData(err));
	}
}

void Computer::cellWasHit(int letter,int number)
{

	m_playerGrid->clearChanges();

	Coordinates coord(letter,number);
	m_playerGrid->hitCell(coord);
	QString message;
	if(m_playerGrid->cellAt(coord).isEmpty())
	{
		message=std::string("re"+std::to_string(coord.get_letter())+std::to_string(coord.get_number())).data();
		emit(signalSendToPlayer(message));
		emit(signalMakeMove());
	}
	else
	{
		if(!m_playerGrid->cellAt(coord).isSunk())
		{
			message=std::string("ro"+std::to_string(coord.get_letter())+std::to_string(coord.get_number())).data();
			emit(signalSendToPlayer(message));
		}
		else
		{
			std::string temporaryString("rd");
			GridModel::changes_container::iterator iter=m_playerGrid->get_changes().begin();
			GridModel::changes_container::iterator end=m_playerGrid->get_changes().end();

			for(;iter!=end;++iter)
			{
				temporaryString+=std::to_string(iter->get_letter());
				temporaryString+=std::to_string(iter->get_number());
			}
			message=temporaryString.data();
			emit(signalSendToPlayer(message));
			if( m_playerGrid->get_1deckerCount()+m_playerGrid->get_2deckerCount()+
				m_playerGrid->get_3deckerCount()+m_playerGrid->get_4deckerCount()==0 )
			{
				emit(signalSendToPlayer("w"));
			}
		}
	}
}

void Computer::resultOccupied(int letter,int number)
{

	m_enemyGrid->cellAt(Coordinates(letter,number)).makeOccupied();
	m_enemyGrid->hitCell(Coordinates(letter,number));
	if(!m_foundShip)
	{
		m_foundShip=true;
		m_FirstFoundShipPart=Coordinates(letter,number);
		m_neighbors=m_enemyGrid->get_neighboringCells(m_FirstFoundShipPart);
	}
	else
	{
		m_foundOrientation=true;
		m_CurrentFoundShipPart=Coordinates(letter,number);
		m_changeDirection=false;
	}
	emit(signalMakeMove());
}

void Computer::resultEmpty(int letter,int number)
{
	m_enemyGrid->cellAt(Coordinates(letter,number)).makeEmpty();
	m_enemyGrid->hitCell(Coordinates(letter,number));
	if(m_foundOrientation)
	{
		m_changeDirection=true;
	}
}

void Computer::resultShipSunk(QString& str)
{

	QString::iterator iter=str.begin()+2;
	QString::iterator end=str.end();
	Coordinates coord;
	while(iter!=end)
	{
		coord.set_letter(iter->digitValue());
		++iter;
		coord.set_number(iter->digitValue());
		++iter;
		m_enemyGrid->cellAt(coord).reveal();
		m_enemyGrid->cellAt(coord).makeOccupied();
		m_enemyGrid->cellAt(coord).hit();
		m_enemyGrid->cellAt(coord).sink();
	}

	//determined ship type
	GridModel::ShipType type;
	GridModel::ShipOrientation orientation;
	switch (str.size())
			{
			case 4:
				type=GridModel::ShipType_SingleDecker;
				break;
			case 6:
				type=GridModel::ShipType_TwoDecker;
				break;
			case 8:
				type=GridModel::ShipType_ThreeDecker;
				break;
			case 10:
				type=GridModel::ShipType_FourDecker;
				break;
			default:
				break;
			}
	//determined ship orientation
	if(type!=GridModel::ShipType_SingleDecker && str[2].digitValue()==str[4].digitValue())
	{
		orientation=GridModel::ShipOrientation_Vertical;
	}
	else
	{
		orientation=GridModel::ShipOrientation_Horisontal;
	}
	//determined ship's first deck
	coord=Coordinates(str[2].digitValue(),str[3].digitValue());
	//made oreol around ship.computer will not hit them
	m_enemyGrid->makeOreol(coord,type,orientation);

	//return to initial state
	m_foundShip=false;
	m_foundOrientation=false;
	m_changeDirection=false;
	m_orientation=-1;
	m_neighbors.clear();

	emit(signalMakeMove());
}

void Computer::slotMakeMove()
{
	//Computer makes his move after 1.5 second. Then player can see sequence of moves
	
		QTime time;
		time.start();
		while(time.elapsed() < 1500){}
		if(m_foundShip)//if we have found ship earlier
		{
			tryToKillFoundShip();
		}
		else
		{
			makeRandomMove();
		}
}

