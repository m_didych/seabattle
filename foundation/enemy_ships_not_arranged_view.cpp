#include "stdafx.h"

EnemyShipsNotArrangedView::EnemyShipsNotArrangedView(QWidget *parent /*=0*/)
{	
	 m_quit=new QPushButton("&Quit");

	 connect(m_quit,SIGNAL(clicked()),this,SLOT(slotQuit()));

	//Layout setup
    QGridLayout* layout = new QGridLayout; 
    layout->addWidget(new QLabel("<H1> Your opponent has not arranged his ships yet. </H1>"),1,0,3,3);
	layout->addWidget(m_quit,5,2);
    setLayout(layout);
}

EnemyShipsNotArrangedView::~EnemyShipsNotArrangedView()
{
}

void EnemyShipsNotArrangedView::slotQuit()
{
	emit(signalReturnToMenu());
}