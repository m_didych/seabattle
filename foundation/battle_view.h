#ifndef BATTLE_VIEW_H
#define BATTLE_VIEW_H


class BattleView : public QWidget
{
	Q_OBJECT

public:
	typedef std::map<std::string,QBrush> BrushColection;

	BattleView(GridModel* playerGrid,GridModel* enemyGrid,QWidget *parent = 0);
	~BattleView();
	void displayPlayerGrid();
	void displayEnemyGrid();

	

private:
	void displayCell(GridModel* grid, QTableWidget* gridView,const Coordinates& coord);
	void displayGrid(GridModel* grid, QTableWidget* gridView);
	void displayGridChanges(GridModel* grid, QTableWidget* gridView);

signals:
	void signalReturnToMenu();
	void signalCellClicked(int number,int letter);

	public slots:
		void SlotShowWhoseMove(bool isMyMove);
		void slotDisplayEnemyGridChanges();
		void slotDisplayPlayerGridChanges();
		void slotDisplayEnemyCell(int letter,int number);

	private slots:
		void slotCellClicked(int number,int letter);
		void slotQuit();


private:
	QTableWidget* m_playerGridView;
	QTableWidget* m_enemyGridView;
	GridModel* m_playerGrid;
	GridModel* m_enemyGrid;

	BrushColection m_brushes;

	QLabel* m_info;
	QPushButton* m_quit;
};
#endif //BATTLE_VIEW