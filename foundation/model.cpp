#include "stdafx.h"

GridModel::GridModel():m_4deckerCount(0),
	                   m_3deckerCount(0),
					   m_2deckerCount(0),
					   m_1deckerCount(0)
{
	m_grid=cell_container(size*size,Cell());
}

bool GridModel::coordinatesAreValid(const Coordinates& coord)
{
	return coord.get_number()>=0 && coord.get_number()<size &&
		   coord.get_letter()>=0 && coord.get_letter()<size;
}

bool GridModel::shipsAreArranged()const
{
	return m_1deckerCount+m_2deckerCount+m_3deckerCount+m_4deckerCount==10;
}

void GridModel::clearChanges()
{
	m_changes.clear();
}
void GridModel::addToChanges(Coordinates& coord)
{
		m_changes.insert(coord);
}
GridModel::changes_container& GridModel::get_changes()
{
	return m_changes;
}
void GridModel::set_changes(changes_container changes)
{
	m_changes=changes;
}

Cell& GridModel::cellAt(const Coordinates& coord)
{
	return m_grid[(coord.get_number()*size)+(coord.get_letter()%size)];
}

void GridModel::Ship::addDeck(Coordinates& coord)
{
		m_decks.push_back(coord);
		++m_decksNumber;
}

void GridModel::reset()
{
	m_1deckerCount=0;
	m_2deckerCount=0;
	m_3deckerCount=0;
	m_4deckerCount=0;
	m_ships.clear();
	m_changes.clear();

	cell_container::iterator iter=m_grid.begin();
	cell_container::iterator end=m_grid.end();
	for(;iter!=end;++iter)
	{
		(*iter).makeNotRevealed();
		(*iter).makeEmpty();
		(*iter).makeNotHit();
		(*iter).makeNotSunk();
		(*iter).makeNotHighlited();
	}
}

void GridModel::makeOreol(const Coordinates& coord,ShipType type,ShipOrientation orientation)
{
	int width;
	int height;
	Coordinates oreol=coord;

		if(orientation==ShipOrientation_Horisontal)
		{
			width=type;
			height=1;
		}
		else
		{
			width=1;
			height=type;
		}

		if(coord.get_letter()!=0)
			{
				oreol.set_letter(coord.get_letter()-1);
			}
		while(oreol.get_letter()<size && oreol.get_letter()<coord.get_letter()+width+1)
		{
			if(coord.get_number()!=0)
			{
				oreol.set_number(coord.get_number()-1);
			}
			else
			{
				oreol.set_number(coord.get_number());
			}
			while(oreol.get_number()<size && oreol.get_number()<coord.get_number()+height+1)
			{
				if(!cellAt(oreol).isRevealed())
				{
					cellAt(oreol).makeEmpty();
					cellAt(oreol).reveal();
					//////////////////
					addToChanges(oreol);
					//////////////////
				}
				oreol.set_number(oreol.get_number()+1);
			}
			oreol.set_letter(oreol.get_letter()+1);
		}
}

bool GridModel::addShip(const Coordinates& coord,ShipType type,ShipOrientation orientation)
{   
	bool result=false;

	if(IsValidShipPosition(coord,type,orientation))
	{
		Coordinates current=coord;
		Ship ship;

		ship.setType(type);

		if(orientation==ShipOrientation_Horisontal)
		{
			for(int i=0;i!=type;++i)
			{
				current.set_letter(coord.get_letter()+i);
				cellAt(current).makeOccupied();
				cellAt(current).reveal();
				ship.addDeck(current);
				cellAt(current).setShipIndex(m_ships.size());
				///////////////
				addToChanges(current);
				///////////////
			}
		}
		else
		{
			for(int i=0;i!=type;++i)
			{
				current.set_number(coord.get_number()+i);
				cellAt(current).makeOccupied();
				cellAt(current).reveal();
				ship.addDeck(current);
				cellAt(current).setShipIndex(m_ships.size());
				///////////////
				addToChanges(current);
				///////////////
			}
		}
		m_ships.push_back(ship);

		makeOreol(coord,type,orientation);

		switch (type)
		{
		case ShipType_SingleDecker:
			++m_1deckerCount;
			break;
		case ShipType_TwoDecker:
			++m_2deckerCount;
			break;
		case ShipType_ThreeDecker:
			++m_3deckerCount;
			break;
		case ShipType_FourDecker:
			++m_4deckerCount;
			break;
		default:
			break;
		}

		result=true;
	}
	return result;
}

bool GridModel::IsValidShipPosition(const Coordinates& coord,ShipType type,ShipOrientation orientation)
{
	bool result=true;
	Coordinates current=coord;
	if(!coordinatesAreValid(coord))
	{
		result=false;
	}

	if(orientation==ShipOrientation_Horisontal)
	{
		for(int i=0;i!=type;++i)
		{
			current.set_letter(coord.get_letter()+i);
			if(current.get_letter()>=size || cellAt(current).isRevealed()){result=false;}
		}
	}
	else
	{
		for(int i=0;i!=type;++i)
		{
			current.set_number(coord.get_number()+i);
			if(current.get_number()>=size ||cellAt(current).isRevealed()){result=false;}
		}
	}

	return result;
}

void GridModel::makeCellsHighlited(const Coordinates& coord,int quantity,ShipOrientation orientation)
{
	Coordinates current=coord;

	if(orientation==ShipOrientation_Horisontal)
	{
		for(int i=0;i!=quantity;++i)
		{
			current.set_letter(coord.get_letter()+i);
			if(current.get_letter()<size)
			{
				cellAt(current).makeHighlited();
				///////////////
					addToChanges(current);
				///////////////
			}
		}
	}
	else
	{
		for(int i=0;i!=quantity;++i)
		{
			current.set_number(coord.get_number()+i);//move to next number
			if(current.get_number()<size)
			{
				cellAt(current).makeHighlited();
				///////////////
					addToChanges(current);
					///////////////
			}
		}
	}
}

bool GridModel::showSelectedShipPosition(const Coordinates& coord,ShipType type,ShipOrientation orientation)
{
	makeCellsHighlited(coord,type,orientation);
	return IsValidShipPosition(coord,type,orientation);
}

void GridModel::hitCell(Coordinates& coord)
{
	if(coordinatesAreValid(coord))
	{
		cellAt(coord).hit();
		cellAt(coord).reveal();
		///////////////////////
		addToChanges(coord);
		////////////////////////
		int index=cellAt(coord).getShipIndex();
		if(index!=-1)
		{
			Ship* ship=&m_ships[index];
			ship->decrementDecksNumber();
			if(ship->getDecksNumber()<1)
			{
				Ship::deck_container decks=ship->getDecks();

				for(int i=0;i!=decks.size();++i)
				{
					cellAt(decks[i]).sink();
					cellAt(decks[i]).setShipIndex(-1);
					///////////////
					addToChanges(decks[i]);
					///////////////
				}

				switch (ship->getType())
				{
				case ShipType_SingleDecker:
					--m_1deckerCount;
					break;
				case ShipType_TwoDecker:
					--m_2deckerCount;
					break;
				case ShipType_ThreeDecker:
					--m_3deckerCount;
					break;
				case ShipType_FourDecker:
					--m_4deckerCount;
					break;
				default:
					break;
				}
			}
		}
	}
}

void GridModel::pseudoRandomArrangement()
{
	addShip(Coordinates(2,4),ShipType_FourDecker,ShipOrientation_Vertical);
	addShip(Coordinates(5,1),ShipType_ThreeDecker,ShipOrientation_Horisontal);
	addShip(Coordinates(6,4),ShipType_ThreeDecker,ShipOrientation_Vertical);
	addShip(Coordinates(9,3),ShipType_TwoDecker,ShipOrientation_Vertical);
	addShip(Coordinates(2,2),ShipType_TwoDecker,ShipOrientation_Horisontal);
	addShip(Coordinates(6,8),ShipType_TwoDecker,ShipOrientation_Horisontal);

	addShip(Coordinates(1,9),ShipType_SingleDecker,ShipOrientation_Vertical);
	addShip(Coordinates(4,5),ShipType_SingleDecker,ShipOrientation_Vertical);
	addShip(Coordinates(9,1),ShipType_SingleDecker,ShipOrientation_Horisontal);
	addShip(Coordinates(9,7),ShipType_SingleDecker,ShipOrientation_Horisontal);
}

void GridModel::arrangeRandomlyShipsOfType(GridModel::ShipType type)
{
	int index=0;
	int count=0;
	Coordinates coord(0,0);
	GridModel::ShipOrientation orientation=GridModel::ShipOrientation_Horisontal;
		for(int i=0;i!=5-type;++i)
		{
			do
			{
				if(rand()%2)
				{
					orientation=GridModel::ShipOrientation_Horisontal;
				}
				else
				{
					orientation=GridModel::ShipOrientation_Vertical;
				}
				index=rand()%(size*size);
				coord.set_letter(index%size);
				coord.set_number((index-index%size)/size);

				++count;
			}
			while(count<10000 && !addShip(coord,type,orientation));
		}
}

void GridModel::arrangeShipsRandomly()
{
	srand((unsigned)time(NULL));
	int count=0;
	while(count<10000 && !shipsAreArranged())
	{
		reset();
		arrangeRandomlyShipsOfType(GridModel::ShipType_SingleDecker);
		arrangeRandomlyShipsOfType(GridModel::ShipType_TwoDecker);
		arrangeRandomlyShipsOfType(GridModel::ShipType_ThreeDecker);
		arrangeRandomlyShipsOfType(GridModel::ShipType_FourDecker);
		++count;
	}
	if(!shipsAreArranged())
	{
		pseudoRandomArrangement();
	}
}

GridModel::coordinates_container GridModel::get_notRevealedCells()
{
	Coordinates coord(0,0);
	coordinates_container result;
	for(int i=0;i!=size;++i)
	{
		coord.set_letter(i);
		for(int j=0;j!=size;++j)
		{
			coord.set_number(j);
			if(!cellAt(coord).isRevealed())
			{
				result.push_back(coord);
			}
		}
	}
	return result;
}

GridModel::coordinates_container GridModel::get_neighboringCells(Coordinates& coord)
{
	coordinates_container result;

	if(coordinatesAreValid(coord))
	{
		if(coord.get_letter()<size-1)
		{
			result.push_back(Coordinates(coord.get_letter()+1,coord.get_number()));
		}
		if(coord.get_number()<size-1)
		{
			result.push_back(Coordinates(coord.get_letter(),coord.get_number()+1));
		}
		if(coord.get_letter()>0)
		{
			result.push_back(Coordinates(coord.get_letter()-1,coord.get_number()));
		}
		if(coord.get_number()>0)
		{
			result.push_back(Coordinates(coord.get_letter(),coord.get_number()-1));
		}
	}
	return result;
}

