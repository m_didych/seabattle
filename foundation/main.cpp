#include "stdafx.h"
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	srand((unsigned)time(NULL));
	QApplication a(argc, argv);
	GridModel playerGrid;
	GridModel enemyGrid;
	SBWindow w(&playerGrid,&enemyGrid);
	w.show();
	return a.exec();
}
