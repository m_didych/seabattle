#ifndef SELECT_MODE_VIEW_H
#define SELECT_MODE_VIEW_H




class SelectModeView : public QWidget
{
	Q_OBJECT

public:
	typedef std::map<std::string,QBrush> BrushColection;

	SelectModeView(QWidget *parent = 0);
	~SelectModeView();

	void displayCell(const Coordinates& coord);
	void displayGrid();
	void displayGridChanges();


signals:
	void signalCreateGame();
	void signalJoinGame();
	void signalSinglePlayer();


	private slots:
		void slotCreateGame();
		void slotJoinGame();
		void slotSinglePlayer();

private:
	void SelectShipType(GridModel::ShipType type);

private:
	QPushButton* m_createGame;
	QPushButton* m_joinGame;
	QPushButton* m_singlePlayer;

};

#endif // SELECT_MODE_VIEW_H