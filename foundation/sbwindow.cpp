#include "stdafx.h"

SBWindow::SBWindow(GridModel* playerGrid,GridModel* enemyGrid,QWidget *parent)
															: QMainWindow(parent),
															m_playerGrid(playerGrid),
															m_enemyGrid(enemyGrid)
															
{
	m_selectModeView=new SelectModeView(this);
	m_arrangeShipView=new ArrangeShipView(playerGrid,this);
	m_battleView=new BattleView(playerGrid,enemyGrid,this);
	m_createGameView=new CreateGameView(this);
	m_enemyShipsNotArrangedView=new EnemyShipsNotArrangedView(this);
	m_joinGameView=new JoinGameView(this);

	m_view=new QStackedWidget(this);
	m_view->addWidget(m_selectModeView);
	m_view->addWidget(m_arrangeShipView);
	m_view->addWidget(m_battleView);
	m_view->addWidget(m_createGameView);
	m_view->addWidget(m_enemyShipsNotArrangedView);
	m_view->addWidget(m_joinGameView);

	setCentralWidget(m_view);
	m_view->setCurrentWidget(m_selectModeView);

    m_controller=new Controller(playerGrid,enemyGrid,this);

	
	//views--->main window connections
	   //Change view
		connect(m_selectModeView,SIGNAL(signalCreateGame()),this,SLOT(slotCreateGame()));
		connect(m_selectModeView,SIGNAL(signalJoinGame()),this,SLOT(slotSetJoinGameView()));
		connect(m_joinGameView,SIGNAL(signalJoinCreatedGame(QString&,int)),this,SLOT(slotJoinCreatedGame(QString&,int)));
		connect(m_selectModeView,SIGNAL(signalSinglePlayer()),this,SLOT(slotSinglePlayer()));

		//Return to menu
		connect(m_createGameView,SIGNAL(signalReturnToMenu()),this,SLOT(slotSetSelectModeView()));
		connect(m_enemyShipsNotArrangedView,SIGNAL(signalReturnToMenu()),this,SLOT(slotSetSelectModeView()));
		connect(m_battleView,SIGNAL(signalReturnToMenu()),this,SLOT(slotSetSelectModeView()));
		connect(m_joinGameView,SIGNAL(signalReturnToMenu()),this,SLOT(slotSetSelectModeView()));
		connect(m_arrangeShipView,SIGNAL(signalReturnToMenu()),this,SLOT(slotSetSelectModeView()));

	//controller--->main window conections
	connect(m_controller,SIGNAL(signalArrangeShips()),this,SLOT(slotSetArrangeShipView()));
	connect(m_controller,SIGNAL(signalStartGame()),this,SLOT(slotSetBattleView()));
	connect(m_controller,SIGNAL(signalEnemyShipsNotArranged()),this,SLOT(slotEnemyShipsNotArranged()));
	connect(m_controller,SIGNAL(signalYouWon(bool)),this,SLOT(slotGameOver(bool)));
	connect(m_controller,SIGNAL(signalInvalidData(QString)),this,SLOT(slotError(QString)));

	//main window--->controller conections
	connect(this,SIGNAL(signalSetMove(bool)),m_controller,SLOT(slotSetMove(bool)));

	//views--->controller conections
	connect(m_arrangeShipView,SIGNAL(signalReady()),m_controller,SLOT(slotPlayerReady()));
	connect(m_battleView,SIGNAL(signalCellClicked(int,int)),m_controller,SLOT(slotHitEnemyCell(int,int)));
	
	//controller--->battle view connections
	connect(m_controller,SIGNAL(signalDisplayEnemyGridChanges()),m_battleView,SLOT(slotDisplayEnemyGridChanges()));
	connect(m_controller,SIGNAL(signalDisplayPlayerGridChanges()),m_battleView,SLOT(slotDisplayPlayerGridChanges()));
	connect(m_controller,SIGNAL(signalDisplayEnemyCell(int,int)),m_battleView,SLOT(slotDisplayEnemyCell(int,int)));
	connect(m_controller,SIGNAL(signalShowIsMyMove(bool)),m_battleView,SLOT(SlotShowWhoseMove(bool)));
}

SBWindow::~SBWindow()
{

}

void SBWindow::slotSetBattleView()
{
	m_battleView->displayPlayerGrid();
	m_battleView->displayEnemyGrid();
	m_view->setCurrentWidget(m_battleView);
}

void SBWindow::slotSetArrangeShipView()
{
	m_arrangeShipView->reset();
	m_view->setCurrentWidget(m_arrangeShipView);
}

void SBWindow::slotSetJoinGameView()
{
	m_view->setCurrentWidget(m_joinGameView);
}

void SBWindow::slotSetSelectModeView()
{
	m_controller->reset();
	m_view->setCurrentWidget(m_selectModeView);
}

void SBWindow::slotGameOver(bool youWon)
{
	QString message;
	if(youWon)
	{
		message="Congratulations! You won!";
	}
	else
	{
		message="You lost the game :(";
	}
	QMessageBox::information(0, "Game over", message);
	slotSetSelectModeView();
}

void SBWindow::slotEnemyShipsNotArranged()
{
	m_view->setCurrentWidget(m_enemyShipsNotArrangedView);
}

void SBWindow::slotCreateGame()
{
	Server* server=new Server();
	QThread* thread = new QThread();

	server->moveToThread(thread);

	//server--->main window connections
	connect(server, SIGNAL(signalError(QString)), this, SLOT(slotError(QString)));
	connect(server, SIGNAL(signalPort(quint16)), this, SLOT(slotPort(quint16)));

	//thread-->server connections
	connect(thread, SIGNAL(started()), server, SLOT(process()));//creates TCP Server

	//server-->thread connections
	connect(server, SIGNAL(signalFinished()), thread, SLOT(quit()));

	//controller-->server connections
	connect(m_controller,SIGNAL(signalSendToEnemy(QString)),server,SLOT(slotSendResponse(QString)));
	connect(m_controller,SIGNAL(signalYouWon(bool)),server,SLOT(slotQuit()));

	//server-->controller connections
	connect(server,SIGNAL(signalNewData(QString)),m_controller,SLOT(slotInterpretData(QString)));

	//views--->server connections
	connect(m_createGameView,SIGNAL(signalReturnToMenu()),server,SLOT(deleteLater()));//??
	connect(m_enemyShipsNotArrangedView,SIGNAL(signalReturnToMenu()),server,SLOT(slotQuit()));///??
	connect(m_arrangeShipView,SIGNAL(signalReturnToMenu()),server,SLOT(slotQuit()));
	connect(m_battleView,SIGNAL(signalReturnToMenu()),server,SLOT(slotQuit()));//??


	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

	emit(signalSetMove(true));
	thread->start();
}

void SBWindow::slotJoinCreatedGame(QString& IP,int port)
{
	Client* client=new Client(IP,port);
	QThread* thread = new QThread();

	client->moveToThread(thread);

	//client--->main window connections
	connect(client, SIGNAL(signalError(QString)), this, SLOT(slotError(QString)));

	//thread-->client connections
	connect(thread, SIGNAL(started()), client, SLOT(process()));//creates socket

	//client-->thread connections
	connect(client, SIGNAL(signalFinished()), thread, SLOT(quit()));

	//controller-->client connections
	connect(m_controller,SIGNAL(signalSendToEnemy(QString)),client,SLOT(slotSendResponse(QString)));
	connect(m_controller,SIGNAL(signalYouWon(bool)),client,SLOT(slotQuit()));
	
	//client-->controller connections
	connect(client,SIGNAL(signalNewData(QString)),m_controller,SLOT(slotInterpretData(QString)));

	//views--->client connections
	connect(m_enemyShipsNotArrangedView,SIGNAL(signalReturnToMenu()),client,SLOT(slotQuit()));///??
	connect(m_arrangeShipView,SIGNAL(signalReturnToMenu()),client,SLOT(slotQuit()));
	connect(m_battleView,SIGNAL(signalReturnToMenu()),client,SLOT(slotQuit()));//??

	emit(signalSetMove(false));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();
}

void SBWindow::slotSinglePlayer()
{
	Computer* computer=new Computer();
	QThread* thread = new QThread();

	computer->moveToThread(thread);

	//thread-->computer connections
	connect(thread, SIGNAL(started()), computer, SLOT(process()));

	//computer-->thread connections
	connect(computer, SIGNAL(signalFinished()), thread, SLOT(quit()));

	//controller-->computer connections
	connect(m_controller,SIGNAL(signalSendToEnemy(QString)),computer,SLOT(slotInterpretData(QString)));
	connect(m_controller,SIGNAL(signalYouWon(bool)),computer,SLOT(deleteLater()));

	//computer-->controller connections
	connect(computer,SIGNAL(signalSendToPlayer(QString)),m_controller,SLOT(slotInterpretData(QString)));

	//views--->computer connections
	connect(m_enemyShipsNotArrangedView,SIGNAL(signalReturnToMenu()),computer,SLOT(deleteLater()));///??
	connect(m_arrangeShipView,SIGNAL(signalReturnToMenu()),computer,SLOT(deleteLater()));
	connect(m_battleView,SIGNAL(signalReturnToMenu()),computer,SLOT(deleteLater()));//??

	emit(signalSetMove(true));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();

}

void SBWindow::slotError(QString message)
{

	QMessageBox::information(0, "Information", message);
	slotSetSelectModeView();
}

void SBWindow::slotPort(quint16 port)
{
	m_createGameView->setPort(port);
	m_view->setCurrentWidget(m_createGameView);	
}

