#ifndef CLIENT_H
#define CLIENT_H



class Client : public QObject {
Q_OBJECT
private:
    void sendToServer(QString message);


public:
	  Client(QString& IP,int port=55555,QObject* parent = 0) ;

signals:
	  void signalFinished();
	  void signalError(QString err);
	  void signalNewData(QString message);


public slots:
	void process();
    void slotReadyRead();
    void slotError(QAbstractSocket::SocketError);
	void slotSendResponse (QString message);
	void slotQuit();


private:
	QTcpSocket* m_tcpSocket;
    quint16     m_nextBlockSize;
	QString m_IP;
	int m_port;
};
#endif  //CLIENT_H